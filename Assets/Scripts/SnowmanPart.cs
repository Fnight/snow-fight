﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowmanPart : MonoBehaviour
{
    public GameObject PartentSnowman;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Kill()
    {
        PartentSnowman.GetComponent<Snowman>().Kill();
    }
}
