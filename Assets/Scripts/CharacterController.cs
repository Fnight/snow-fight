﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterController : MonoBehaviour
{
    public GameObject CurrentGun;
    public GameObject DefaultGun;
    public Transform WeaponPosition;
    private float lastTimeChangeWeapon = 0f;
    
    public CinemachineVirtualCamera Camera;
    public LevelManager LevelManager;
    private CinemachineBasicMultiChannelPerlin channelPerlin;
    private float shakeTimeElapsed = 0f;
    public float ShakeAmplitude = 1f;

    public float MovementSpeed = 7.0f;
    public float AccelRate = 20.0f;
    public float DecelRate = 20.0f;
    public float AirborneAccel = 5.0f;
    public float JumpSpeed = 7.0f;
    public float FudgeExtra = 0.5f;
    public float MaximumSlope = 45.0f;
    public float CrouchSize = 1.8f;
    public float RadiusToTake = 1f;

    private bool grounded = false;
    private bool isKill = false;
    private bool isCrouch = false;

    //Unity Components
    private Rigidbody rb;
    private CapsuleCollider coll;
    private float standSize;

    // Temp vars
    private float inputX;
    private float inputY;
    private Vector3 movement;

    // Acceleration or deceleration
    private float acceleration;

    /*
     * Keep track of falling
     */
    private bool falling;

    /*
     * Jump state var:
     * 0 = hit ground since last jump, can jump if grounded = true
     * 1 = jump button pressed, try to jump during fixedupdate
     * 2 = jump force applied, waiting to leave the ground
     * 3 = jump was successful, haven't hit the ground yet (this state is to ignore fudging)
    */
    private byte doJump;

    // Average normal of the ground i'm standing on
    private Vector3 groundNormal;

    // If we're touching a dynamic object, don't prevent idle sliding
    private bool touchingDynamic;

    // Was i grounded last frame? used for fudging
    private bool groundedLastFrame;

    // The objects i'm colliding with
    private List<GameObject> collisions;

    // All of the collision contact points
    private Dictionary<int, ContactPoint[]> contactPoints;

    /*
     * Temporary calculations
     */
    private float halfPlayerHeight;
    private float fudgeCheck;

    private float
        bottomCapsuleSphereOrigin; // transform.position.y - this variable = the y coord for the origin of the capsule's bottom sphere

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        coll = GetComponent<CapsuleCollider>();

        standSize = coll.height;

        channelPerlin = Camera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        LevelManager.EndGame += PlayerDead;

        movement = Vector3.zero;

        grounded = false;
        groundNormal = Vector3.zero;
        touchingDynamic = false;
        groundedLastFrame = false;

        collisions = new List<GameObject>();
        contactPoints = new Dictionary<int, ContactPoint[]>();

        // do our calculations so we don't have to do them every frame
        CapsuleCollider capsule = (CapsuleCollider) coll;
        Debug.Log(capsule);
        halfPlayerHeight = capsule.height * 0.5f;
        fudgeCheck = halfPlayerHeight + FudgeExtra;
        bottomCapsuleSphereOrigin = halfPlayerHeight - capsule.radius;

        PhysicMaterial controllerMat = new PhysicMaterial();
        controllerMat.bounciness = 0.0f;
        controllerMat.dynamicFriction = 0.0f;
        controllerMat.staticFriction = 0.0f;
        controllerMat.bounceCombine = PhysicMaterialCombine.Minimum;
        controllerMat.frictionCombine = PhysicMaterialCombine.Minimum;
        capsule.material = controllerMat;

        // just in case this wasn't set in the inspector
        rb.freezeRotation = true;
    }


    void FixedUpdate()
    {
        if (isKill) return;

        // check if we're grounded
        RaycastHit hit;
        grounded = false;
        groundNormal = Vector3.zero;

        foreach (ContactPoint[] contacts in contactPoints.Values)
            for (int i = 0; i < contacts.Length; i++)
                if (contacts[i].point.y <= rb.position.y - bottomCapsuleSphereOrigin &&
                    Physics.Raycast(contacts[i].point + Vector3.up, Vector3.down, out hit, 1.1f, ~0) &&
                    Vector3.Angle(hit.normal, Vector3.up) <= MaximumSlope)
                {
                    grounded = true;
                    groundNormal += hit.normal;
                }

        if (grounded)
        {
            // average the summed normals
            groundNormal.Normalize();

            if (doJump == 3)
                doJump = 0;
        }
        else if (doJump == 2)
            doJump = 3;

        // get player input
        inputX = Input.GetAxis("Horizontal");
        inputY = Input.GetAxis("Vertical");

        // limit the length to 1.0f
        float length = Mathf.Sqrt(inputX * inputX + inputY * inputY);

        if (length > 1.0f)
        {
            inputX /= length;
            inputY /= length;
        }

        if (grounded && doJump != 3)
        {
            if (falling)
            {
                // we just landed from a fall
                falling = false;
            }

            // align our movement vectors with the ground normal (ground normal = up)
            Vector3 newForward = transform.forward;
            Vector3.OrthoNormalize(ref groundNormal, ref newForward);

            Vector3 targetSpeed = Vector3.Cross(groundNormal, newForward) * inputX * MovementSpeed +
                                  newForward * inputY * MovementSpeed;

            length = targetSpeed.magnitude;
            float difference = length - rb.velocity.magnitude;

            // avoid divide by zero
            if (Mathf.Approximately(difference, 0.0f))
                movement = Vector3.zero;

            else
            {
                // determine if we should accelerate or decelerate
                if (difference > 0.0f)
                    acceleration = Mathf.Min(AccelRate * Time.deltaTime, difference);

                else
                    acceleration = Mathf.Max(-DecelRate * Time.deltaTime, difference);

                // normalize the difference vector and store it in movement
                difference = 1.0f / difference;
                movement = new Vector3((targetSpeed.x - rb.velocity.x) * difference * acceleration,
                    (targetSpeed.y - rb.velocity.y) * difference * acceleration,
                    (targetSpeed.z - rb.velocity.z) * difference * acceleration);
            }

            if (doJump == 1)
            {
                // jump button was pressed, do jump    
                movement.y = JumpSpeed - rb.velocity.y;
                doJump = 2;
            }
            else if (!touchingDynamic && Mathf.Approximately(inputX + inputY, 0.0f) && doJump < 2)
                // prevent sliding by countering gravity... this may be dangerous
                movement.y -= Physics.gravity.y * Time.deltaTime;

            rb.AddForce(new Vector3(movement.x, movement.y, movement.z), ForceMode.VelocityChange);
            groundedLastFrame = true;
        }
        else
        {
            // not grounded, so check if we need to fudge and do air accel

            // fudging
            if (groundedLastFrame && doJump != 3 && !falling)
            {
                // see if there's a surface we can stand on beneath us within fudgeCheck range
                if (Physics.Raycast(transform.position, Vector3.down, out hit,
                        fudgeCheck + (rb.velocity.magnitude * Time.deltaTime), ~0) &&
                    Vector3.Angle(hit.normal, Vector3.up) <= MaximumSlope)
                {
                    groundedLastFrame = true;

                    // catches jump attempts that would have been missed if we weren't fudging
                    if (doJump == 1)
                    {
                        movement.y += JumpSpeed;
                        doJump = 2;
                        return;
                    }

                    // we can't go straight down, so do another raycast for the exact distance towards the surface
                    // i tried doing exsec and excsc to avoid doing another raycast, but my math sucks and it failed horribly
                    // if anyone else knows a reasonable way to implement a simple trig function to bypass this raycast, please contribute to the thead!
                    if (Physics.Raycast(
                        new Vector3(transform.position.x, transform.position.y - bottomCapsuleSphereOrigin,
                            transform.position.z), -hit.normal, out hit, hit.distance, ~0))
                    {
                        rb.AddForce(hit.normal * -hit.distance, ForceMode.VelocityChange);
                        return; // skip air accel because we should be grounded
                    }
                }
            }

            // if we're here, we're not fudging so we're defintiely airborne
            // thus, if falling isn't set, set it
            if (!falling)
                falling = true;


            // air accel
            if (!Mathf.Approximately(inputX + inputY, 0.0f))
            {
                // note, this will probably malfunction if you set the air accel too high... this code should be rewritten if you intend to do so

                // get direction vector
                movement = transform.TransformDirection(new Vector3(inputX * AirborneAccel * Time.deltaTime, 0.0f,
                    inputY * AirborneAccel * Time.deltaTime));

                // add up our accel to the current velocity to check if it's too fast
                float a = movement.x + rb.velocity.x;
                float b = movement.z + rb.velocity.z;

                // check if our new velocity will be too fast
                length = Mathf.Sqrt(a * a + b * b);
                if (length > 0.0f)
                {
                    if (length > MovementSpeed)
                    {
                        // normalize the new movement vector
                        length = 1.0f / Mathf.Sqrt(movement.x * movement.x + movement.z * movement.z);
                        movement.x *= length;
                        movement.z *= length;

                        // normalize our current velocity (before accel)
                        length = 1.0f / Mathf.Sqrt(rb.velocity.x * rb.velocity.x + rb.velocity.z * rb.velocity.z);
                        Vector3 rigidbodyDirection = new Vector3(rb.velocity.x * length, 0.0f, rb.velocity.z * length);

                        // dot product of accel unit vector and velocity unit vector, clamped above 0 and inverted (1-x)
                        length = (1.0f - Mathf.Max(
                                      movement.x * rigidbodyDirection.x + movement.z * rigidbodyDirection.z, 0.0f)) *
                                 AirborneAccel * Time.deltaTime;
                        movement.x *= length;
                        movement.z *= length;
                    }

                    // and finally, add our force
                    rb.AddForce(new Vector3(movement.x, 0.0f, movement.z), ForceMode.VelocityChange);
                }
            }

            groundedLastFrame = false;
        }

        if (lastTimeChangeWeapon + 0.3f < Time.time)
        {
            if (Input.GetMouseButton(1))
            {
                var colliders = Physics.OverlapSphere(transform.position, RadiusToTake);
                if (colliders.Any(item => item.CompareTag("Weapon")))
                {
                    foreach (var item in colliders)
                    {
                        if (item.gameObject.CompareTag("Weapon"))
                        {
                            if (CurrentGun.name.Contains("Snowfire"))
                            {
                                Destroy(CurrentGun);
                            }

                            GameObject parent = item.transform.parent.gameObject;
                            GameObject obj = item.gameObject;
                            obj.GetComponent<Rigidbody>().isKinematic = true;
                            obj.GetComponent<Collider>().enabled = false;
                            parent.transform.SetParent(WeaponPosition, true);
                            parent.transform.localPosition = Vector3.zero;
                            parent.transform.localRotation = Quaternion.identity;
                            obj.transform.localPosition = Vector3.zero;
                            obj.transform.localRotation = Quaternion.Euler(-90, 90, 0);
                            CurrentGun = parent;
                            lastTimeChangeWeapon = Time.time;
                            break;
                        }
                    }
                }
                else
                {
                    if (!CurrentGun.name.Contains("Snowfire"))
                    {
                        var o = CurrentGun.transform.GetChild(0).gameObject;
                        o.GetComponent<Rigidbody>().isKinematic = false;
                        o.GetComponent<Collider>().enabled = true;
                        CurrentGun.transform.SetParent(null);
                        o.GetComponent<Rigidbody>().AddForce(CurrentGun.transform.TransformVector(Vector3.forward) * 3, ForceMode.Impulse);
                        var defaultGun = Instantiate(DefaultGun, Vector3.zero, Quaternion.identity);
                        defaultGun.transform.SetParent(WeaponPosition);
                        defaultGun.transform.localPosition = Vector3.zero;
                        defaultGun.transform.localRotation = Quaternion.identity;
                        CurrentGun = defaultGun;
                        lastTimeChangeWeapon = Time.time;
                    }
                }
            }
        }
    }

    void Update()
    {
        // check for input here
        if (groundedLastFrame && Input.GetButtonDown("Jump"))
        {
            doJump = 1;
        }

        if (Input.GetMouseButton(0) && CurrentGun != null)
        {
            CurrentGun.GetComponent<ISnowgun>().MakeFire("Ball");
        }

        //TODO: crouch need to fix 
        // if (Input.GetKeyDown(KeyCode.LeftShift) && !isCrouch)
        // {
        //     isCrouch = true;
        //     coll.height = CrouchSize;
        // }
        //
        // if (Input.GetKeyUp(KeyCode.LeftShift) && isCrouch)
        // {
        //     isCrouch = false;
        //     coll.height = standSize;
        // }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        if (shakeTimeElapsed > 0)
        {
            channelPerlin.m_AmplitudeGain = ShakeAmplitude;
            shakeTimeElapsed -= Time.deltaTime;
        }
        else
        {
            channelPerlin.m_AmplitudeGain = 0;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        collisions.Add(collision.gameObject);
        contactPoints.Add(collision.gameObject.GetInstanceID(), collision.contacts);

        if (!collision.gameObject.isStatic)
        {
            touchingDynamic = true;
        }

        if (doJump == 3)
        {
            doJump = 0;
        }

        if (collision.gameObject.CompareTag("Enemy_Ball") && !isKill)
        {
            TakeDamage(0.5f);
            LevelManager.PlayerTakeDamage();
        }
    }

    private void TakeDamage(float time)
    {
        shakeTimeElapsed = time;
    }

    private void PlayerDead()
    {
        isKill = true;
        rb.freezeRotation = false;
    }

    void OnCollisionStay(Collision collision)
    {
        contactPoints[collision.gameObject.GetInstanceID()] = collision.contacts;
    }

    void OnCollisionExit(Collision collision)
    {
        touchingDynamic = false;

        for (int i = 0; i < collisions.Count; i++)
        {
            if (collisions[i] == collision.gameObject)
            {
                collisions.RemoveAt(i--);
            }
            else if (!collisions[i].isStatic)
            {
                touchingDynamic = true;
            }
        }

        contactPoints.Remove(collision.gameObject.GetInstanceID());
    }
}