﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public GameObject LifesObject;
    public GameObject HeartPrefab;
    
    public int PlayerLifes = 3;
    public Action EndGame;

    private List<GameObject> hearts = new List<GameObject>();
    
    void Start()
    {
        for (int i = 0; i < PlayerLifes; i++)
        {
            var heart = Instantiate(HeartPrefab, Vector3.zero, Quaternion.identity);
            heart.transform.SetParent(LifesObject.transform, false);
            hearts.Add(heart);
        }
    }

    public void PlayerTakeDamage()
    {
        PlayerLifes--;
        var heart = hearts[hearts.Count - 1];
        hearts.Remove(heart);
        Destroy(heart);
        if (PlayerLifes <= 0)
        {
            EndGame?.Invoke();
        }
    }
}
