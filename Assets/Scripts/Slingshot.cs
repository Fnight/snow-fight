﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slingshot : MonoBehaviour, ISnowgun
{
    public GameObject Snowball;

    public float Delay = 0.5f;
    public float Force = 1f;
    
    private float lastShotTime = 0f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MakeFire(string tag)
    {
        if (lastShotTime + Delay < Time.time)
        {
            var snowball = Instantiate(Snowball, transform.position, Quaternion.identity);
            snowball.GetComponent<Rigidbody>().AddForce(transform.TransformVector(Vector3.forward) * Force);
            snowball.tag = tag;
            lastShotTime = Time.time;
        }
    }
}
