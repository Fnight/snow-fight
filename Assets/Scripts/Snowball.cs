﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snowball : MonoBehaviour
{
    public float DelayDestroyBalls = 4f;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DestroyMe(DelayDestroyBalls, gameObject));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            other.gameObject.GetComponent<SnowmanPart>().Kill();
            Destroy(gameObject);
        }
    }
    
    IEnumerator DestroyMe(float time, GameObject o)
    {
        yield return new WaitForSeconds(time);
        Destroy(o);
    }
}
