﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Snowman : MonoBehaviour
{

    public GameObject[] Parts;
    public float Force = 1f;
    public GameObject Weapon;
    public float DistanceToFire = 100f;
    private float DistanceToSee = 100f;

    private NavMeshAgent navMeshAgent;
    private Transform target;
    private bool isKill = false;
    
    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isKill)
        {
            if (CheckTarget(DistanceToSee))
            {
                navMeshAgent.SetDestination(target.position);    
            }
            
            if (CheckTarget(DistanceToFire))
            {
                Weapon.GetComponent<ISnowgun>().MakeFire("Enemy_Ball");
            }
        }
    }

    private bool CheckTarget(float distance)
    {
        return !navMeshAgent.Raycast(target.position, out _) &&
               Vector3.Distance(transform.position, target.position) < distance;
    }

    public void Kill()
    {
        isKill = true;
        Destroy(Weapon);
        foreach (var part in Parts)
        {
            part.GetComponent<Rigidbody>().isKinematic = false;
            part.GetComponent<Rigidbody>().AddForce(Random.value * Force, Random.value * Force, 0);
        }
    }
}
